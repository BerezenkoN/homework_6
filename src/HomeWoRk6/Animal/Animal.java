package HomeWoRk6.Animal;

/**
 * Created by user on 15.10.2016.
 */
public abstract class Animal {

    String name;
    int countOfLegs;
    int countOfWings;
    String color;
    Type type;

    abstract void voice();

}
