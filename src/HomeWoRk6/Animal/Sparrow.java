package HomeWoRk6.Animal;

/**
 * Created by user on 15.10.2016.
 */
 class Sparrow extends Animal implements Runnable, Flyable {
    public Sparrow (String name, String color){
        this.name = name;
        this.color = color;
        this.type = Type.BIRD;
        this.countOfLegs = 2;
        this.countOfWings = 2;
    }

    @Override
    void voice(){
        System.out.println(" My name is " + this.name + "." +
                " My color is " + this.color + "." +
                " I have " + this.countOfLegs +
                " legs. " +
                " I have " + this.countOfWings +
                " wings. " + run() + fly() + " Who am I? ");
    }

    @Override
    public String run(){
        return " I can run! ";
    }
    @Override
    public String fly(){
        return " I can fly! ";
    }
}
