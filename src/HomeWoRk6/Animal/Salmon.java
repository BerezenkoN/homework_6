package HomeWoRk6.Animal;

/**
 * Created by user on 15.10.2016.
 */
 class Salmon extends Animal implements Swimable {
    public Salmon (String name, String color){
        this.name = name;
        this.color = color;
        this.type = Type.FISH;


    }

    @Override
    void voice(){
        System.out.println(" My name is " + this.name + "." +
                " My color is " + this.color + "." +
                " I have " + this.countOfLegs +
                " legs. " +
                " I have " + this.countOfWings +
                " wings. " + swim() + " Who am I? ");
    }

    @Override
    public String swim(){
        return " I can swim! ";
    }
}
