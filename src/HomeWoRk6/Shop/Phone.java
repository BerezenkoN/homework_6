package HomeWoRk6.Shop;

/**
 * Created by user on 15.10.2016.
 */
 class Phone {
    private String producer;
    private String model;
    private int numberOfSim = 1;
    private boolean hasCamera = false;
    private String color;
    private PhoneType phoneType;

    public Phone(String producer,String model, int numberOfSim, boolean hasCamera, String color, PhoneType phoneType ){
        this.producer = producer;
        this.model = model;
        this.numberOfSim = numberOfSim;
        this.hasCamera = hasCamera;
        this.color = color;
        this.phoneType = phoneType;
    }
    public void setProducer(String producer){
        this.producer = producer;
    }
    public void setModel(String model){
        this.model = model;
            }
    public void setNumberOfSim(int numberOfSim){
        this.numberOfSim = numberOfSim;
    }
    public void setHasCamera(boolean hasCamera){
        this.hasCamera = hasCamera;
    }
    public void setColor(String color){
        this.color = color;
    }
    public void setPhoneType(PhoneType phoneType){
        this.phoneType = phoneType;
    }
@Override
    public boolean equals(Object o) {
    if(this == o) return true;
    if(!(o instanceof Phone)) return false;

    Phone phone = (Phone) o;
    if(numberOfSim != phone.numberOfSim) return false;
    if(hasCamera != phone.hasCamera) return false;
    if(!producer.equals(phone.producer)) return false;
    if(!model.equals(phone.model)) return  false;
    return phoneType == phone.phoneType;
}

    @Override
    public int hashCode() {
        int result = producer.hashCode();
        result = 31 * result + model.hashCode();
        result = 31 * result + numberOfSim;
        result = 31 * result + (hasCamera ? 1 : 0);
        result = 31 * result + phoneType.hashCode();

        return result;

    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer( " Phone (");
        sb.append("producer = '").append(producer).append('\'');
        sb.append(", model = '").append(model).append('\'');
        sb.append(", numberOfSim = ").append(numberOfSim);
        sb.append(", hasCamera = ").append(hasCamera);
        sb.append(", color = '").append(color).append('\'');
        sb.append(", phoneType = ").append(phoneType.name());
        sb.append(')');
        return  sb.toString();


    }


}
