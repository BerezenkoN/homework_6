package HomeWoRk6.Shop;

import java.util.Arrays;

/**
 * Created by user on 16.10.2016.
 */
public class Shop {

    private Phone [] phonesInStock = new Phone[0];

    public Phone[] getPhonesInStock() {
        return phonesInStock;
    }

    void newDelivery(Phone[] phonesDelivery) {
        final int oldTelLength = phonesInStock.length;
        final int newTelLength = phonesDelivery.length + oldTelLength;
        phonesInStock = Arrays.copyOf(phonesInStock, newTelLength);
        System.arraycopy(phonesDelivery, 0, phonesInStock, oldTelLength, phonesDelivery.length);
    }
    int countOfSearchPhone(Phone searchPhone) {
        int count = 0;
        for(Phone phone :  phonesInStock)
            if(phone.equals(searchPhone))
                count++;
        return count;
    }

    void printPhonesInStock() {
        System.out.println(" Phones in Stock");
        for(Phone phone :  phonesInStock)
            System.out.println(phone);
    }

}
